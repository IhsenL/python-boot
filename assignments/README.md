# Python Bootcamp Assigment Handbook

## How to install python, pip, pipx and poetry
To unify and simplify the setup a bundle with a python version and required tools was added to this git repo.

1. Download python 3.12.XX from https://www.python.org/downloads/ . Make sure you install pip and "Add python to environment variables"
2. install pipx and poetry:
````
python -m pip install pipx
pipx install poetry
````
3. Add poetry to your environemnt "Path" variable. You'll find the peotry.exe in your pipx directory "pipx\venvs\poetry\Scripts"
4. Run in python-bootcamp/assigments/ 
````
poetry install
````


## How to run the unit tests
Run in your assigmentXX directory
````
poetry run pytest tests
````
When you run it in an not yet worked on assigment you should see that all tests failed. You know a task is acceptable finished when the regarding unittests pass.

To execute a single test case, for example because this is the one which you cannot get passed you can run
````
pytest tests/test_mod.py::test_func
````

**!Do not modify the unittests unless it is explicitly part of a task. If you want or must write additional unittests create new python modules (aka .py files)!**

Further information can be found in the official docs: [https://docs.pytest.org/en/8.2.x/how-to/usage.html](https://docs.pytest.org/en/8.2.x/how-to/usage.html)

## How to add libraries 
[poetry](https://python-poetry.org/docs/) is used to manage dependencies. 
To add dependencies use the poetry [add](https://python-poetry.org/docs/cli/#add) command. You need to execute the command in a directory where a poetry project is already initialized (same as with git in a git repo directory). 

Please have a look into the [command list ](https://python-poetry.org/docs/cli/) and the [managing dependencies](https://python-poetry.org/docs/managing-dependencies/) section for more advanced dependency management

## What is this poetry.lock and pyproject.toml file for?
