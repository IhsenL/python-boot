# -*- coding: utf-8 -*-
from assignment02.typing import untyped_function, strings_to_numbers, define_types_for_this_function


def test_untyped_function():
    ...


def test_strings_to_numbers():
    ...


def test_define_types_for_this_function():
    ...
