# -*- coding: utf-8 -*-

type Cadena = list | tuple | range | set | frozenset


def get_diff_elements_between_list(l1: Cadena, l2: Cadena) -> list:
    """Task01
    Write a function which takes two Cadena objects and returns all values which are not in both objects
    There should be no duplicates in the result. 

    Args:
        l1 (Cadena): first Cadena object
        l2 (Cadena): second Cadena object

    Returns: 
        list: a list which contains all elements of l1 and l2 which are either in one of them but not in both
    """

    ...


def search_by_ingredient(recipes: dict) -> dict:
    """Task02
    Iterate over the ingredients of different recipes. Create a new dict which contains each found ingredient as a key and a list of recipes which use this ingredient
    Example result:
    Input:
    {
        'apple pie': ['apple', 'flour'],
        'fruit salad': ['apple', 'banana'],
        'pudding': ['milk', 'starch', 'choco']
    }
    Output: 
    {
        'apple': ['apple pie', 'fruit salad'] 
        'milk': ['pancakes', 'pudding', 'hot choco']
    }
    
    Args:
        recipes (dict): recipes with lists of ingriedients

    Returns
        dict: by ingredient searchable dict
    """

    ...


def switch_keys_with_values(input: dict) -> dict:
    """Task03
    Write a function that switches a the keys with the values of a dict. Example:
    {                {
      'a': 1,          1: 'a',
      'b': 2,    ->    2: 'b',
      'c': 3           3: 'c'
    }                }
    Hint 1: It might happen that a value is a mutable container, which is not hashable. Make sure to convert it to its immutable container pendant
    Hint 2: You can ignore nested structures

    Args:
        input(dict): dict of which keys and values will be switched

    Returns: 
        dict: a dict which has all the values of the input dict as keys and all keys as values
    """

    ...


def append_to_immutable(input: frozenset | tuple, new_item: any) -> frozenset | tuple:
    """Task04

    Args:
        input (frozenset | tuple): immutable container which must be extended
        new_item (any): any item which can be added to a container object

    Returns: 
        frozenset | tuple: the same type as the variable "input" has. The result is the input with the new_item added/appended
    """

    ...


def dict_deepcopy(input: dict) -> dict:
    """Task05 (Optional)
    Implement your own deepcopy function to make a copy of full a dictionary
    Do not use the deepcopy() function from the copy package!

    Args:
        input (dict): dict of which a deepcopy will be created

    Returns: 
        dict: new dict object which has no references to the input dict
    """

    ...
